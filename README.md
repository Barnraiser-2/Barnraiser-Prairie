# Barnraiser-Prairie

## About Barnraiser

You can read more about the background of this repository here: [About_Barnraiser](About_Barnraiser.md)

## About Prairie

This repository is based upon the latest version prairie_20080922.tar.gz found at http://barnraiser.org/prairie. It was planned as a successor of the AROUNDMe Personal identity and AROUNDMe Identity server, which you can also find around here. They were discontinued with the start of Prairie when they already had reached version 1.2 and 1.3. Prairie itself never made it beyond alpha stage 0.1. 

The following is the original description from http://barnraiser.org/prairie

----

## Introducing Prairie
Forget all those different username and password combinations once and for all with Prairie; our Internet identity server.

Prairie is a lightweight OpenID based Internet identity server. Instead of registering at every web site with different username and password combinations you use your identity server to log you in.

Features
  - Simple profile webpage (Internet identity).
  - Install as a single user or a service to host many separate users.
  - Contact form.
  - DH-SHA1 support.
  - DH-SHA256 support.
  - OpenID 1.1 complient.
  - OpenID 2.0 complient.
  - Themed "skins" which can be easily downloaded and added.
  - Multi-lingual.
  - Free (GPL) software license.

## A case study; Identity page of Tom Calthrop
￼
One example of Prairie is the identity page of Tom Calthrop (maintainer of Prairie) and the founding director of Barnraiser.

The identity page is using an out of the box copy of Prairie.

http://tom.calthrop.info

## Technical considerations
Prairie requires a web server running either Apache 1.3/2.x or IIS5/IIS6 with PHP5.x installed including GD library, Curl and BCMath.

For multiple instances you will require wildcard sub-domains.

---